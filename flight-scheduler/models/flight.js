'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class flight extends Model {
    static associate(models) {
    }
  };
  flight.init({
    droneId: DataTypes.STRING,
    corpId: DataTypes.STRING,
    scheduledAt: {
      type:DataTypes.DATE,
      defaultValue:DataTypes.NOW
    },
    status: {
      type:DataTypes.ENUM('scheduled', 'initiated', 'failed'),
      defaultValue:"scheduled"
    }
  }, {
    sequelize,
    modelName: 'flight',
  });
  return flight;
};