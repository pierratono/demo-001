'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('flights', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      droneId: {
        type: Sequelize.STRING,
        allowNull:false
      },
      corpId: {
        type: Sequelize.STRING,
        allowNull:false
      },
      scheduledAt: {
        type: Sequelize.DATE
      },
      status: {
        type: Sequelize.ENUM('scheduled', 'initiated', 'failed'),
        defaultValue:"scheduled"
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue:Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue:Sequelize.NOW

      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('flights');
  }
};