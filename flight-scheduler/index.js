const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const {flight} = require("./models")

const app = express();

const corsOptions = {
    origin: "*"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
    flight.findAll().then(response => res.json(response)).catch(
        err=>res.json({
            error: err
        })
    )
});

app.get("/getFlight/:status", (req, res) => {

    const status= req.params.status;

    flight.findAll({where:{
            status: status}}).then(response => res.json(response)).catch(
        err=>res.json({
            error: err
        })
    )

});

app.put("/updateFlight", (req, res) => {

    const{id,status} = req.body;

    flight.update({status:status},{where:{
            id: id}}).then(response => res.json({message:"success"})).catch(
        err=>res.json({
            error: err
        })
    )

});

// set port, listen for requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
