const express = require("express");
const firebase = require("firebase");
const firebaseConfig = require("./config/firebase").firebaseConfig;
const cors = require("cors");

const Gpio = require("onoff").Gpio;
const LED = new Gpio(4, "out"); //Initialize GPIO 4

const app = express(); // Define App
app.use(cors()); // Enable cross-origin
app.use(express.urlencoded({ extended: true })); //Enable Extended Url
app.use(express.json()); // Enable support for JSON

firebase.initializeApp(firebaseConfig); // Initialize Firebase

//Initial State Data
let initState = "";
let dataList = {};
let smallServer = 0;

//Define Blinking LED Function
const blinkLED = () => {
  if (LED.readSync() === 0) {
    LED.writeSync(1);
  } else {
    LED.writeSync(0);
  }
};

// Listen to any change on firebase
let listenDrone = firebase.database().ref("/");
listenDrone.on("child_changed", (snapshot) => {
  const data = snapshot.val();
  dataList = data;
  smallServer = Number(new Date()); //Record Time in Millis After receiving changes
  console.log("=============================");
  console.log("DRONE STATUS CHANGED -- Toggling LED");
  console.log({ delay: Number(new Date()) }); //Print millis
  console.log("=============================");
  blinkLED(); // Call Blinking LED Function
});

//Get new list of drones
app.get("/drones", (req, res) => {
  res.json(dataList);
});
//Get last delay in millis
app.get("/delay", (req, res) => {
  res.json(smallServer);
});
//Get local saved drones
app.get("/local", (req, res) => {
  res.send(initState);
});
//Default 80/ serve
app.get("/", (req, res) => {
  res.json({
    server: "running",
    firebase: true,
  });
});
// Start server
app.listen(5002, () => {
  console.log("Server has started on port: 5002");
});
