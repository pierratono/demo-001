const express = require("express");
const firebase = require("firebase");
const cors = require("cors");
const firebaseConfig = require("./config/firebase").firebaseConfig;

const app = express(); // Define App
app.use(cors()); // Enable cross-origin
app.use(express.urlencoded({ extended: true })); //Enable Extended Url
app.use(express.json()); // Enable support for JSON
app.set("view engine", "ejs"); // Initialize Render

firebase.initializeApp(firebaseConfig); // Initialize Firebase
let database = firebase.database(); //Instance Firebase Realtime Database

//Define Method to add drone
const addDrone = (corpId, droneId) => {
  firebase.database().ref(`corporations/${corpId}/drones/${droneId}`).set({
    status: "idle",
  });
};
// addDrone("SP001e", "MK-1");
// addDrone("SP001e", "MK-2");
// addDrone("SP001e", "MK-3");
// addDrone("TKN002e", "MK-1");
// addDrone("TKN002e", "MK-2");
// addDrone("TKN002e", "MK-3");

// Index page
app.get("/", function (req, res) {
  //Define state
  let listDrones = [];
  let listCorps = [];
  //Load current DB data
  database
    .ref("corporations/")
    .get()
    .then((snapshot) => {
      let dtl = []; //Array holder
      let dyl = snapshot.val(); // Data from response
      listDrones.push(snapshot.val());
      //Provide line to line link
      for (corp in listDrones) {
        let corps = listDrones[corp];
        listCorps = Object.keys(corps);
        for (drone in corps) {
          let drones = corps[drone];
          for ([key, value] of Object.entries(drones)) {
            dtl.push({
              [drone]: Object.keys(value),
            });
          }
        }
      }

      res.render("pages/index", {
        list: dtl,
        data: dyl,
      });
    });
});
//Get remote drones
app.get("/drones", (req, res) => {
  database
    .ref("corporations/")
    .get()
    .then((snapshot) => {
      if (snapshot.exists()) {
        res.json(snapshot.val());
      } else {
        res.json({
          error: "No data available",
        });
      }
    })
    .catch((error) => {
      res.json({
        error: error,
      });
    });
});
//Start flight
app.post("/start_flight", (req, res) => {
  const buttonPressed = Number(new Date()); //Store millis when button pressed
  const { corpId, droneId, status } = req.body; //Get data from body
  database
    .ref("corporations/")
    .child(corpId)
    .child("drones")
    .child(droneId)
    .set({
      status: status,
    })
    .then((snapshot) => {
      if (snapshot.exists()) {
        res.json({ success: true }, snapshot.val());
      } else {
        res.json({
          error: "No data available",
        });
      }
    })
    .catch((error) => {
      res.json({
        aws: buttonPressed, //Return millis
        error: error,
      });
    });
});

//Start server
app.listen(5001, () => {
  console.log("Server has started on port: 5001");
});
