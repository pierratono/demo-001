const express = require("express");
const request = require("request");
const firebase = require("firebase");
const firebaseConfig = require("./config/firebase").firebaseConfig;

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

firebase.initializeApp(firebaseConfig);

let initState = "";
let dataList = {};
let smallServer = 0;

let listenDrone = firebase.database().ref("/");
listenDrone.on("child_changed", (snapshot) => {
  const data = snapshot.val();
  dataList = data;
  smallServer = Number(new Date());
  console.log("=============================");
  console.log("DRONE STATUS CHANGED");
  console.log({ delay: Number(new Date()) });
  console.log("=============================");

  //   request.post("http://localhost:5001/set_data").form(data);
  //   request
  //     .post("http://localhost:5001/set_delay")
  //       .form({ smallServer: Number(new Date()) });
  for (corp in data) {
    let corps = data[corp];
    for (drone in corps) {
      let drones = corps[drone];
      for ([key, value] of Object.entries(drones)) {
        const updateURL = `corporations/${corp}/drones/${key}/${value.status}`;
        initState += updateURL;
      }
    }
  }
});

// addDrone("SP001e", "MK-1");
// addDrone("SP001e", "MK-2");
// addDrone("SP001e", "MK-3");
// addDrone("TKN002e", "MK-1");
// addDrone("TKN002e", "MK-2");
// addDrone("TKN002e", "MK-3");

app.get("/drones", (req, res) => {
  res.json(dataList);
});

app.get("/delay", (req, res) => {
  res.json(smallServer);
});

app.get("/local", (req, res) => {
  res.send(initState);
});

app.get("/", (req, res) => {
  res.json({
    server: "running",
    firebase: true,
  });
});

app.listen(5002, () => {
  console.log("Server has started on port: 5002");
});
