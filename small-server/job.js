const cron = require("node-cron");
const axios = require("axios");
const moment = require("moment")

cron.schedule("* * * * * *", () => {
  axios({
    method: 'get',
    url: 'http://143.198.2.16:8000/getFlight/scheduled',
  })
      .then((response) =>{
          const flights = response.data

          for (let i = 0; i < flights.length; i++) {
              const flight = flights[i]

              let now = moment().utc()
              let scheduleTime = moment(flight.scheduledAt).utc()
              let diff = scheduleTime.diff(now,'seconds');

              // console.log(diff)

              if(diff<10 && diff>0){

              axios({
                  method: 'put',
                  url: 'http://143.198.2.16:8000/updateFlight',
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : {"id":flight.id.toString(),"status":"initiated"}
              })
                  .then(function (response) {
                      console.log(JSON.stringify(response.data));
                  })
                  .catch(function (error) {
                      console.log(error);
                  });
              }
          }
      })
      .catch( (error)=> {
        console.log(error);
      });
});
